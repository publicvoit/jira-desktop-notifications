#!/bin/bash
set -o nounset
set -o pipefail
## set -o errexit  ## enabling this command would cause the shell script NOT to catch jira-query.py issues

## ################################################################################
## This shell script is using jira-query.py from https://gitlab.com/publicvoit/jira-query
## for querying Jira according to the JIRA QUERY below.
## If this query returns at least one newly opened ticket, it notifies the user via notify-send on the desktop.
##
## Add this shell script to your cron daemon to automate things. See my note on
## MYJIRAQUERYCOMMAND below if you follow that advice.
##
## Author: Karl Voit
## More information: https://gitlab.com/publicvoit/jira-desktop-notifications
## ################################################################################


## ################################################################################
## Configuration:                                               (express yourself!)

JQUERY='assignee = currentUser() and status != Done'

## the file that keeps the stderr output of jira-query.py (just for the reporting; gets deleted at the end)
JQUERYLOGFILE=$(mktemp)

## notify-send example from the author's setup according to the defaults below:
##           ABC Project
##           42 assigned tickets in total
##           New: ABC-23 ABC-117

## notify-send category:
NOTIFY_CATEGORY="ABC Project"

## notify-send summary which is prepended by the total number of matching tickets for the query:
NOTIFY_SUMMARY_POSTFIX="assigned tickets in total"

## notify-send body prefix which will be extended by the new keys:
NOTIFY_BODY_PREFIX="New: "

## if you use appendorgheading (for Emacs Org-mode), this parameter controls the Org-mode category:
APPENDORGHEADING_CATEGORY="mycategory"

## this file holds the list of keys found for the comparison on the next iteration:
LOGFILE_FOR_LAST_RESULT="/tmp/jira-results-my-assigned-tickets.log"

## For using cron daemon (which usually has a very limited PATH), you should 
## probably use a full path variable for MYJIRAQUERYCOMMAND instead of the
## previous check. If you want to look in PATH instead, just remove following line:
MYJIRAQUERYCOMMAND="${HOME}/src/jira-query/jira-query.py"

## If there is an error with the server, only one error message will be generated. For that, we're
## going to need an indicator file. If that file is found, further notificatons are omitted as long
## as the error persists.
ERROR_INDICATOR_FILE="/tmp/jira-notification-error-indicator-file.txt"

## Enables and disables debugging output:   (remove or add a comment character accordingly)
#DEBUG="yes"
DEBUG="no"

## ################################################################################
## Let's check some things first ...  (you should know what you're doing from here)

## if MYJIRAQUERYCOMMAND is not set manually (from above), look for jira-query.py in PATH:
if [ "x${MYJIRAQUERYCOMMAND}" = "x" ]; then
    MYJIRAQUERYCOMMAND=$(which jira-query.py)
    if [ ! -x "${MYJIRAQUERYCOMMAND}" ] ; then
        echo "I could not find the executable for \"jira-query.py\" in your PATH. Please make sure it's installed and part of your PATH."
        exit 1
    fi
fi
    
MYNOTIFYSEND=$(which notify-send)
if [ ! -x "${MYNOTIFYSEND}" ]; then
    echo "WARNING: I could not find the executable for \"notify-send\". Please make sure it's installed to get desktop notifications. Using stdout instead."
fi

MYAPPENDORGHEADING=$(which appendorgheading)
if [ ! -x "${MYAPPENDORGHEADING}" ]; then
    echo "I could not find the executable for \"appendorgheading\" (for users of Emacs Org-mode). I'm not using it for error reporting unless you install from https://github.com/novoid/appendorgheading. Using stdout for error messages instead."
fi

## ################################################################################
## And here, the actual magic begins ...       (this turned out to be more complicated than anticipated)

debug()
{
    [ ${DEBUG} = "yes" ] && echo "DEBUG: $@"
}

KEYS=$( "${MYJIRAQUERYCOMMAND}" --keys "${JQUERY}" 2> ${JQUERYLOGFILE} )
MYRETURNVALUE=${?}   ## please make sure that there is NOTHING in-between the jira-query.py call and the MYRETURNVALUE line.

debug "MYRETURNVALUE=[${MYRETURNVALUE}]"

## handle case where jira-query.py did not execute properly:
if (( ${MYRETURNVALUE} != 0 )); then

    ## write nice error message using https://github.com/novoid/appendorgheading if found:
    if [ -x "${MYAPPENDORGHEADING}" ] ; then
        if [ ! -f "${ERROR_INDICATOR_FILE}" ]; then
            ${MYAPPENDORGHEADING} --title "${0}: return value of not jira-query.py not zero" \
                                  --filecontent "${JQUERYLOGFILE}" \
                                  --properties "category:${APPENDORGHEADING_CATEGORY}" \
                                  --section "file:${0}"
        else
            ## ordinary error message for people not using appendorgheading:
            echo "Something happened when executing: jira-query.py --keys \"${JQUERY}\"   (please do fix the quoting within the JQuery string)"
        fi
    fi

    ## additionally, notify via notify-send (if found):
    if [ -x "${MYNOTIFYSEND}" ]; then
        if [ -f "${ERROR_INDICATOR_FILE}" ]; then
            ## non-critical notification (because only the first one will be critical)
            ${MYNOTIFYSEND} --category="${NOTIFY_CATEGORY}" "${NOTIFY_SUMMARY_POSTFIX}" "jira-query.py returned an error since $(cat ${ERROR_INDICATOR_FILE}). Please fix."
        else
            ## the first error launches a critical notification:
            ${MYNOTIFYSEND} --urgency=critical --category="${NOTIFY_CATEGORY}" "${NOTIFY_SUMMARY_POSTFIX}" "jira-query.py returned an error. Please fix."
        fi
    fi
    
    /bin/date +%Y-%m-%dT%H.%M > "${ERROR_INDICATOR_FILE}"  ## create indicator file so that we only create one single critical notification (and only non-critical later-on)
    exit 1
fi

## remove old error indicator file (if found) in case the query was successful:
rm -f "${ERROR_INDICATOR_FILE}"

## calculate the sum of the tickets:
SUM=$(echo "${KEYS}" | wc -w)

## get previous result for comparison:
if [ -f "${LOGFILE_FOR_LAST_RESULT}" ]; then
    PREVIOUS_KEYS=$(cat "${LOGFILE_FOR_LAST_RESULT}")
    PREVIOUS_SUM=$(echo "${PREVIOUS_KEYS}" | wc -w)
else
    PREVIOUS_KEYS=""
    PREVIOUS_SUM="0"
fi

debug "SUM:         [$SUM]"
debug "PREVIOUS_SUM:[$PREVIOUS_SUM]"
debug "KEYS:         [$KEYS]"
debug "PREVIOUS_KEYS:[$PREVIOUS_KEYS]"

## determining the newly opened tickets:
NEWLY_OPENED_KEYS=''
for KEY in ${KEYS}; do
    debug "comparing KEY \"${KEY}\" from KEYS.    (NEWLY_OPENED_KEYS: $NEWLY_OPENED_KEYS)"
    if ! echo "${PREVIOUS_KEYS}" | grep -q "${KEY}" ; then
        NEWLY_OPENED_KEYS="$NEWLY_OPENED_KEYS ${KEY}"
        debug "KEY not in COMPARISONLIST -> adding   (NEWLY_OPENED_KEYS: $NEWLY_OPENED_KEYS)" 
    else
        debug "KEY is in COMPARISONLIST. Ignoring."     
    fi
done
## strip string from leading and trailing spaces:
NEWLY_OPENED_KEYS=$(echo "${NEWLY_OPENED_KEYS}" | sed 's/ *$//g'  | sed 's/^ *//g')
debug "NEWLY_OPENED_KEYS: [$NEWLY_OPENED_KEYS]"

## notify if new keys exists
if [ ! -z "${NEWLY_OPENED_KEYS}" ]; then
    
    if [ -x "${MYNOTIFYSEND}" ]; then
        ${MYNOTIFYSEND} --urgency=critical --category="${NOTIFY_CATEGORY}" "${SUM} ${NOTIFY_SUMMARY_POSTFIX}" "${NOTIFY_BODY_PREFIX}${NEWLY_OPENED_KEYS}"
    else
        echo "I have found ${SUM} matching jira ticket(s) which is more or changed from last time. Do your thing. New are: ${NEWLY_OPENED_KEYS}"
    fi

fi

## remember sum for next call:
echo "${KEYS}" > "${LOGFILE_FOR_LAST_RESULT}"

if (( ${MYRETURNVALUE} == 0 )); then
    ## remove stderr output file of this call:
    rm "${JQUERYLOGFILE}"
else
    debug "JQUERYLOGFILE with error message: ${JQUERYLOGFILE}"
fi

## this is the end, my friend.
